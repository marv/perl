# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require perl-module systemd-service

MY_PNV="Mail-SpamAssassin-${PV}"

SUMMARY="SpamAssassin is an extensible email filter which is used to identify spam"
DESCRIPTION="
SpamAssassin is a mail filter which attempts to identify spam using a variety
of mechanisms including text analysis, Bayesian filtering, DNS blocklists, and
collaborative filtering databases.SpamAssassin is a mail filter which attempts
to identify spam using a variety of mechanisms including text analysis,
Bayesian filtering, DNS blocklists, and collaborative filtering databases.
"
HOMEPAGE="http://spamassassin.apache.org"
DOWNLOADS="mirror://apache/${PN}/source/${MY_PNV}.tar.bz2"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-lang/perl:=[>=5.8.1][berkdb]
        dev-perl/Digest-SHA1
        dev-perl/HTML-Parser[>=3.43]
        dev-perl/IO-Socket-INET6
        dev-perl/IO-Socket-SSL[>=1.76]
        dev-perl/Net-DNS[>=0.34]
        dev-perl/NetAddr-IP
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    run:
        user/spamd
        group/spamd
    suggestion:
        (
            app-crypt/gnupg                 [[ description = [ The 'sa-update' script requires this to verify updates. ] ]]
            dev-perl/HTTP-Date              [[ description = [ The 'sa-update' script requires this to make HTTP If-Modified-Since GET requests. ] ]]
            dev-perl/libwww-perl            [[ description = [ The 'sa-update' script requires this to make HTTP requests. ] ]]
        ) [[ *group-name = [ sa-update ] ]]
        (
            dev-perl/DBD-mysql              [[ description = [ Needed to use a MySQL database as backend for user configuration data, Bayes storage or other storage ] ]]
            dev-perl/DBI                    [[ description = [ Needed if you want to use an SQL database backend for user configuration data, Bayes storage or other storage ] ]]
        ) [[ *group-name = [ db-mysql ] ]]
        (
            dev-perl/DBD-Pg                 [[ description = [ Needed to use a PostgreSQL database as backend for user configuration data, Bayes storage or other storage ] ]]
            dev-perl/DBI                    [[ description = [ Needed if you want to use an SQL database backend for user configuration data, Bayes storage or other storage ] ]]
        ) [[ *group-name = [ db-postgresql ] ]]
        (
            dev-perl/DBD-SQLite             [[ description = [ Needed to use a SQLite database as backend for user configuration data, Bayes storage or other storage ] ]]
            dev-perl/DBI                    [[ description = [ Needed if you want to use an SQL database backend for user configuration data, Bayes storage or other storage ] ]]
        ) [[ *group-name = [ db-sqlite ] ]]
        dev-perl/Encode-Detect         [[ description = [ Needed for the 'normalize_charset' configsetting (to detect charsets and convert them into Unicode) ] ]]
        dev-perl/IP-Country            [[ description = [ Used by the RelayCountry plugin (not enabled by default) to determine the domain country codes of each relay in the path of an email ] ]]
        dev-perl/Mail-DKIM             [[ description = [ If the the DKIM/DomainKeys plugins are enabled, SA will perform DKIM/Domain Key lookups when a DKIM-Signature header/Domain Key information is present in the message headers ] ]]
        dev-perl/Mail-SPF              [[ description = [ Used to check DNS Sender Policy Framework (SPF) records to fight email address forgery and make it easier to identify spams ] ]]
        dev-perl/Net-Ident             [[ description = [ Needed to use the --auth-ident option to spamd ] ]]
        dev-perl/Razor2[>=2.61]        [[ description = [ Used to check message signatures against Vipul's Razor collaborative filtering network ] ]]
        dev-util/re2c                  [[ description = [ Used by sa-compile to compile rules for regular expressions to speed up scanning ] ]]
"

WORK=${WORKBASE}/${MY_PNV}

PERL_MODULE_SRC_CONFIGURE_PARAMS=( SYSCONFDIR=/etc DATADIR=/usr/share/spamassassin ENABLE_SSL="yes" )

src_prepare() {
    default

    # This test needs network access
    edo rm -f t/dnsbl_subtests.t
}

src_test() {
    esandbox allow_net "unix:${TEMP}satest*/*"
    esandbox allow_net --connect "unix:${TEMP}satest*/*"
    perl-module_src_test
    esandbox disallow_net "unix:${TEMP}satest*/*"
    esandbox disallow_net --connect "unix:${TEMP}satest*/*"
}

src_install() {
    perl-module_src_install

    hereconfd spamd.conf <<EOF
SPAMD_OPTS="--max-children 5 --helper-home-dir --nouser-config"

SPAMD_USER="spamd"
SPAMD_GROUP="spamd"
EOF
    install_systemd_files
}

