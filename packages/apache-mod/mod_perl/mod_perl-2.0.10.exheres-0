# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mod_perl-2.0.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require perl-module

SUMMARY="A persistent embedded Perl interpreter for Apache 2.x"
DESCRIPTION="
The Apache/Perl integration project brings together the full power of the Perl
programming language and the Apache HTTP server. With mod_perl it is possible to
write Apache modules entirely in Perl. In addition, the persistent interpreter
embedded in the server avoids the overhead of starting an external interpreter
and the penalty of Perl start-up time.
"
HOMEPAGE="https://perl.apache.org"
DOWNLOADS="mirror://apache/perl/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/dist/${PN}-$(ever range 1-2)-current/Changes"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs/$(ever range 1-2)/index.html [[ lang = en ]]"

LICENCES="Apache-1.1"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# sandbox-incompatible tests (2.0.9-rc3)
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-lang/perl:=
        www-servers/apache[>=2.4.0][apache_modules:authn_file][apache_modules:authz_groupfile][apache_modules:authn_default][apache_modules:authz_default][apache_modules:authz_owner][apache_modules:authz_user]
    test:
        dev-perl/CGI[>=3.45]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.0.7-sneak-tmpdir.patch
)

DEFAULT_SRC_INSTALL_PARAMS=(
    DESTDIR=""
    MODPERL_AP_LIBDIR="${IMAGE}/usr/$(exhost --target)/lib"
    MODPERL_AP_LIBEXECDIR="${IMAGE}/usr/$(exhost --target)/libexec/apache2/modules"
    MODPERL_AP_INCLUDEDIR="${IMAGE}/usr/$(exhost --target)/include/apache2"
    MP_INST_APACHE2=1
    INSTALLDIRS=vendor

)

src_configure() {
    edo perl Makefile.PL \
        PREFIX="${IMAGE}"/usr/$(exhost --target) \
        MP_TRACE=1 \
        MP_DEBUG=1 \
        MP_USE_DSO=1 \
        MP_APXS=/usr/$(exhost --build)/bin/apxs \
        MP_APR_CONFIG=/usr/$(exhost --build)/bin/apr-1-config \
        INSTALLDIRS=vendor </dev/null
}

src_compile() {
    nonfatal emake "${DEFAULT_SRC_COMPILE_PARAMS[@]}" || emake "${DEFAULT_SRC_COMPILE_PARAMS[@]}"
}

src_test() {
    esandbox disable_net

    TMPDIR="${TEMP}" HOME="${TEMP}/" emake -j1 test
}

src_install() {
    default

    fixlocalpod

    insinto /etc/apache2/modules.d
    doins "${FILES}"/conf/apache2-mod_perl-startup.pl
    doins "${FILES}"/conf/75_${PN}.conf
    dodoc -r docs todo

    edo mv "${IMAGE}"/usr/$(exhost --target)/man "${IMAGE}"/usr/share

    # Fix paths.
    for FILE in $(grep -lr paludis "${IMAGE}"/*|grep -v ".so"); do
        sed -i -e "s:${IMAGE}:/:g" ${FILE}
    done
}

